// NAGA CLI - 2023 - megaloadon

use std::{fs::File, io::{Read, Write}};
use zeroize::Zeroize; // Zeroization
use naga::NagaEngine;
use structopt::StructOpt;
use rpassword::read_password;
use colored::Colorize;

#[derive(StructOpt, Debug)]
#[structopt(name = "NAGA CLI", about = "CLI tool for NAGA.")]
struct Cli {
    // Encrypt / Decrypt subcommand
    #[structopt(subcommand)]
    cmd : Command
}

// Base command 
#[derive(StructOpt, Debug)]
enum Command {
    /// Decrypt a file [ naga_cli decrypt <in-file> <out-file> ]
    #[structopt(name = "decrypt")]
    Decrypt {
        infile: String,
        outfile: String
    },

    /// Encrypt a file [ naga_cli encrypt <in-file> <out-file> ]
    #[structopt(name = "encrypt")]
    Encrypt {
        infile: String,
        outfile: String
    }
}

fn main() {
    header();

    // Get the app
    let app = Cli::from_args();

    // Match command
    match &app.cmd {
        Command::Decrypt { infile, outfile } => {
            // Get content
            let mut buf = match file_to_vec(&infile) {
                Ok(c) => c,
                Err(why) => {
                    eprintln!("error : {}", why.to_string().red().bold());
                    std::process::exit(1);
                }
            };

            // Decrypt
            let ngn = NagaEngine::default();

            // Ask for password
            print!("Password please : ");
            let _ = std::io::stdout().flush();
            let mut password : String = match read_password(){
                Ok(s) => s,
                Err(why) => panic!("Error with password : {why:?}"),
            };
            ngn.decrypt(&mut buf, password.as_bytes());
            password.zeroize();
            
            // Write out
            if let Err(why) = vec_to_file(outfile, buf) {
                eprintln!("{}", why.to_string().red().bold());
            }
        }
        Command::Encrypt { infile, outfile } => {
            // Get content
            let mut buf = match file_to_vec(&infile) {
                Ok(c) => c,
                Err(why) => {
                    eprintln!("error : {}", why.to_string().red().bold());
                    std::process::exit(1);
                }
            };

            // Encrypt
            let ngn = NagaEngine::default();
            
            // Ask for password
            print!("Password please : ");
            let _ = std::io::stdout().flush();
            let mut password : String = match read_password(){
                Ok(s) => s,
                Err(why) => panic!("Error with password : {why:?}"),
            };
            ngn.encrypt(&mut buf, password.as_bytes());
            password.zeroize();

            // Write out
            if let Err(why) = vec_to_file(outfile, buf) {
                eprintln!("{}", why.to_string().red().bold());
            }
        }
    }
}

// Easy to handle error
// TODO : Zeroization
fn file_to_vec(file : &str) -> Result<Vec<u8>, String> {
    let mut result = Vec::<u8>::new();
    match File::open(file) {
        Ok(mut f) => match f.read_to_end(&mut result) {
            Ok(_) => Ok(result),
            Err(why) => Err(format!("error : {why:?}")),
        },
        Err(why) => Err(format!("error : {why:?}")),
    }
}

fn vec_to_file(file : &str, mut content : Vec<u8>) -> Result<(),String> {
    match File::create(file) {
        Ok(mut f) => match f.write_all(&content) {
            Ok(_) => Ok(content.zeroize()),
            Err(why) => {
                content.zeroize();
                Err(format!("error : {why}"))
            },
        },
        Err(why) => {
            content.zeroize();
            Err(format!("error : {why}"))
        },
    }
}

fn header() {
    println!("{}\n{}{}",r"
                                         .__  .__ 
    ____ _____     _________        ____ |  | |__|
   /    \\__  \   / ___\__  \     _/ ___\|  | |  |".truecolor(255,150,0).bold(),
r"  |   |  \/ __ \_/ /_/  > __ \_   \  \___|  |_|  |
  |___|  (____  /\___  (____  /____\___  >____/__|
       \/     \//_____/     \/_____/   \/  ".truecolor(255,100,0).bold(), 
    env!("CARGO_PKG_VERSION").to_string().yellow().italic());
}