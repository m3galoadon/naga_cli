# NAGA CLI
naga_cli is a cli tool to encrypt and decrypt files using naga rust library.

## Build
Build using cargo:
`cargo build -r`

## Usage
```
naga_cli encrypt <in-file> <out-file>
naga_cli decrypt <in-file> <out-file>
```

## Help menu
```
NAGA CLI 0.1.0
CLI tool for NAGA.

USAGE:
    naga_cli.exe <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    decrypt    Decrypt a file [ naga_cli decrypt <in-file> <out-file> ]
    encrypt    Encrypt a file [ naga_cli encrypt <in-file> <out-file> ]
    help       Prints this message or the help of the given subcommand(s)
```